/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multiplicationtable;

import java.util.Scanner;

/**
 *
 * @author Saloni
 */
public class MultiplicationTable {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter number: ");
        int num1=sc.nextInt();
        for(int i=0;i<10;i++)
        {
            System.out.println(num1 + " X " + (i+1) + " = " +(num1*(i+1)));
        }
    }
    
}
